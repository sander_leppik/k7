import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Puzzle {

   private static HashMap<Character, Integer> charMap;
   private static List resultsMap;
   private static String addend1;
   private static String addend2;
   private static String sum;
   private static List<Character> charList;

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {
      addend1 = args[0];
      addend2 = args[1];
      sum = args[2];

      resultsMap = new ArrayList();
      charMap = new HashMap<>();
      charList = buildCharacterLibrary(args);


      depthFirstMapBuilder(charList, 0);
      System.out.println("Possible solutions: " + resultsMap.size());
      if(resultsMap.size() > 0){
         System.out.println("One possible solution: " + resultsMap.get(0));
      }
   }

   private static void depthFirstMapBuilder(List<Character> charArr, int depth){
      for (int i = 0; i < charArr.size(); i++) {
         Character tempChar = charArr.get(i);
         charMap.put(tempChar, depth);

         if(charArr.size() == 1){
            solvePuzzle();
         }else{
            List<Character> tempList = new ArrayList<>();
            tempList.addAll(charArr.subList(0, charArr.indexOf(tempChar)));
            tempList.addAll(charArr.subList(charArr.indexOf(tempChar) + 1, charArr.size()));

            depthFirstMapBuilder(tempList, depth + 1);
         }
      }
   }

   private static void solvePuzzle(){
      Long num1 = convertToNum(addend1);
      Long num2 = convertToNum(addend2);
      Long result = convertToNum(sum);

      if(num1 != -1 && num2 != -1 && result != -1 && (num1 + num2 == result)){
         charMap.remove(null);
         if(!resultsMap.contains(charMap)){
            resultsMap.add(charMap.clone());
         }
      }
   }

   private static Long convertToNum(String word){

      StringBuilder sb = new StringBuilder();

         for(int x = 0; x < word.length(); x++){
            Character c = word.charAt(x);
            int num = charMap.get(c);

            if(x == 0 && num == 0){
               return Long.parseLong("-1");
            }

            sb.append(num);
         }
      return Long.parseLong(sb.toString());
   }


   private static List<Character> buildCharacterLibrary(String[] input){
      List<Character> charArr = new ArrayList();
      char temp;

      for (String word:input) {
         for(int x = 0; x < word.length(); x++){
            temp = word.charAt(x);
            if(!charArr.contains(temp)){
               charArr.add(temp);
            }
         }
      }

      while(charArr.size() < 10){
         charArr.add(null);
      }
      return charArr;
   }
}

